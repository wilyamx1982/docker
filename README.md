# Mobile Support

### How to change `PN_APPLICATIONS` settings and applied to current server.

1. **Connect to server via SSH.** Ask the certificate file.

		$ ssh -i ~/.pems/albert-stg.pem ubuntu@worker-stg.albert.today

1. **Edit and save .env file using VIM**
		
		ubuntu@ip-10-0-2-224:~$ vim app/.env
		
1. **Restart docker containers** `rq`, `app` **Wait until process is done.**

		ubuntu@ip-10-0-2-224:~$ cd app
		
		ubuntu@ip-10-0-2-224:~/app$ docker-compose restart rq
		Restarting app_app_1 ... done
		
		ubuntu@ip-10-0-2-224:~/app$ docker-compose restart app
		Restarting app_rq_1 ... done
		
2. **Verify above containers were Up status**

		ubuntu@ip-10-0-2-224:~/app$ docker ps
		CONTAINER ID        IMAGE                                                             COMMAND                  CREATED             STATUS              PORTS                                      NAMES
		a27a7032910b        ecr.us-west-1.amazonaws.com/worker:latest        "./entrypoint.rq.sh"     6 days ago          Up 40 seconds       8000/tcp                                   app_rq_1
		e716c861d84a        ecr.us-west-1.amazonaws.com/nginxworker:latest   "/home/worker/nginx.…"   6 days ago          Up 6 days           0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   app_web_1
		08e58b96d867        ecr.us-west-1.amazonaws.com/worker:latest        "./entrypoint.sh"        6 days ago          Up About a minute   8000/tcp                                   app_app_1
