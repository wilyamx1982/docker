# Docker Commands

[Compose command-line reference](https://docs.docker.com/compose/reference/)

### Explore container

	ubuntu@ip-10-0-2-170:~$ docker ps
	CONTAINER ID        IMAGE                                                          COMMAND                  CREATED             STATUS              PORTS                                      NAMES
	536b05919a3d        994359151799.dkr.ecr.us-west-1.amazonaws.com/nginxapi:latest   "/home/api/nginx.sh"     7 days ago          Up 7 days           0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   app_web_1
	4dfcab8396ee        994359151799.dkr.ecr.us-west-1.amazonaws.com/api:latest        "/app/entrypoint.sh …"   7 days ago          Up 7 days           8000/tcp                                   app_app_1
	ubuntu@ip-10-0-2-170:~$ docker exec -it app_app_1 /bin/bash
	root@4dfcab8396ee:/app#

### Restarting services

	ubuntu@ip-10-0-1-60:~/app$ docker-compose restart rq
	ubuntu@ip-10-0-2-168:~/app$ docker-compose restart app
	
### Up services

	ubuntu@ip-10-0-2-171:~/app$ docker-compose up -d
	Starting app_app_1 ... done
	Starting app_rq_1  ... done
	Starting app_web_1 ... done

### Down services

	ubuntu@ip-10-0-2-171:~/app$ docker-compose down -d
	Stopping sonarqube ... done
	Stopping db        ... done
	Removing sonarqube ... done
	Removing db        ... done
	
### Volumes

	$ docker volume ls
	DRIVER    VOLUME NAME
	local     tawk-github-users_postgresql_data
	local     tawk-github-users_sonarqube_bundled-plugins
	local     tawk-github-users_sonarqube_conf
	local     tawk-github-users_sonarqube_data
	local     tawk-github-users_sonarqube_db
	local     tawk-github-users_sonarqube_extensions

### Logs

	$ docker-compose logs --follow
	
### Cleanup

	$ docker container stop 1124e768985d
	$ docker ps
	
	$ docker rmi 117aa9af20c7 -f
	$ docker images
	
	$ docker container rm 1124e768985d
	$ docker ps

	$ docker image prune -a
	$ docker rmi 117aa9af20c7 -f
	$ docker container prune
	$ docker volume prune
	$ docker system prune
